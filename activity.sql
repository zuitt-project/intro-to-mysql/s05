USE classic_models;

--1.Return the customerName of the customers who are from Philippines
	
	SELECT customerName
	FROM customers
	WHERE country LIKE "%Philippines";
	
--2.Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"

	SELECT contactLastName, contactFirstName
	FROM customers
	WHERE customerName LIKE "%La Rochelle Gifts"; 
	
--3.Return the product name and MSRP of the product names "The Titanic"

	SELECT productName, MSRP
	FROM products
	WHERE productName LIKE "%The Titanic";

-- 4.Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
	
	SELECT firstName, lastName
	FROM employees
	WHERE email LIKE "%jfirrelli@classicmodelcars.com";

--5.Return the names of customers who have no registered state

	SELECT customerName
	FROM customers
	WHERE state IS NULL;

--6.Return the first name, last name, email of the empoyee whose last name is Patterson and first name is Steve

	SELECT firstName, lastName, email
	FROM employees
	WHERE lastName LIKE "%Patterson"
	AND firstName LIKE "%Steve";

--7.Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000

	SELECT customerName, country, creditLimit
	FROM customers
	WHERE country LIKE "%USA"
	AND creditLimit > 3000;

--8.Return the customer names of customers whose customer names don't have 'a' in them
	
	SELECT customerName
	FROM customers
	WHERE customerName NOT LIKE "%a%";

--9.Return the customer numbers of orders whose comments contain the string 'DHL'

	SELECT customerNumber 
	FROM orders
	WHERE comments LIKE "%DHL%";

--10.Return the product lines whose text description mentions the phrase 'state of the art'.
	
	SELECT productLine
	FROM productLines
	WHERE textDescription LIKE "%state of the art%";

--11.Return the countries of customers without duplication
	
	SELECT DISTINCT country FROM customers;

--12.Return the statuses of orders without duplication
	
	SELECT DISTINCT status FROM orders;

--13.Return the customer names and countries of customers whose country is USA, France or Canada

	SELECT customerName, country
	FROM customers
	WHERE country LIKE "%USA"
	OR country LIKE "%FRANCE"
	OR country LIKE "%CANADA";

--14.Return the first name, last name, and office's city of employees whose offices are in Tokyo.

	SELECT employees.firstName, employees.lastName, offices.city
	FROM employees  INNER JOIN offices
	ON employees.officeCode =offices.office_officeCode
	WHERE offices.city = "Tokyo";

--15.Return the customer names of customer who were served by the employee named "Leslie Thompson"

	SELECT customerName 
	FROM customers 
	WHERE salesRepEmployeeNumber 
	IN (SELECT employeeNumber 
		FROM employees 
		WHERE firstName="Leslie" 
		AND lastName="Thompson");

--16.Return the product names and customer name of products ordered by "Baane Mini Imports"

	SELECT  products.productName, customers.customerName 
	FROM products 
	INNER JOIN customers 
	WHERE customers.customerName="Baane Mini Imports";

--17.Return the employees' first names, employees' last name, customers' names, and offices' countries of transaction whose customers and offices are in the same country

	SELECT COUNT(*)
	FROM products
	WHERE productLine 
	IN (SELECT productLine 
		FROM productLines) 
	GROUP BY productLine;

--18.Return the last names and first names of employees being supervised by "Anthony Bow"

	SELECT lastName, firstName
	FROM employees
	WHERE reportsTo 
	IN(SELECT employeeNumber 
		FROM employees 
		WHERE firstName = "Anthony" 
		AND lastName = "Bow");

--19.Return the product name and MSRP of the product with the highest MSRP

	SELECT productName MAX(MSRP) AS "Largest MSRP"
	FROM products; 

--20.Return the number of customers in the UK
	
	SELECT COUNT(customerName)
	FROM customers
	WHERE country LIKE "%UK";

--21.Return the number of products per product line
	
	SELECT COUNT(*)
	FROM products
	WHERE productLine IN (SELECT productLine FROM productLines) GROUP BY productLine;

--22.Return the number of customers served by every employee

	SELECT DISTINCT customerName, salesRepEmployeeNumber
	FROM customers
	ORDER BY salesRepEmployeeNumber ASC;


--23.Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantites less than 1000.

	SELECT productName, quantityInStock
	FROM products
	WHERE productLine LIKE "%planes"
	AND quantityInStock < 1000.00;